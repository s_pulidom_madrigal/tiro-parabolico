var contexto;
function inicializar () {
    let lienzo =
    document.getElementById('miLienzo');
    lienzo.width = window.innerWidth;
    lienzo.height = window.innerHeight;
    contexto = lienzo.getContext('2d');
    contexto.beginPath();
    contexto.lineWidth = 1;
    contexto.strokeStyle = "#CCCCCC";
    contexto.moveTo(0,(window.innerHeight / 2)-100);
    contexto.stroke();
    contexto.translate(0,window.innerHeight/2);
    contexto.scale(1, -1);
}
function vely(){
    let vy= document.getElementById('velocidad').Value;
    let ang=
    document.getElementById('angulo').value;
    let vinicialY=parseInt(vy)*Math.sin(parseInt(ang)*(Math.PI/180))
    

    return vinicialY


}

function velx(){
    let vx=document.getElementById('velocidad').value;
    let ang= document.getElementById('angulo').value;
    let vinicialx=parseInt(vx)*Math.cos(parseInt(ang)*(Math.PI/180));

    return vinicialx
}

    function dibujarcoord(x,y,tpunto){
        var tpunto = parseInt(tpunto);

        contexto.fillStyle = 'green';
        contexto.beginPath();
        contexto.arc(x,y,tpunto, 0, Math.PI * 2, true);
        contexto.fill();
    }
function Borrar(){
    contexto.clearRect(0,-150,window.innerWidth,window.innerHeight);
}

function simular(){
    let vinicialx = velx();
    let vinicialy = vely();

    var y=0,x=0,t=0;

    while(y>=0){
        y = vinicialy*t-0.5*9.8*t*t;
        x = vinicialx*t;
        console.log(+x+ "," +y+ "  en " +t )
        t+=0.002;
        dibujarcoord(x+30,y-100,4);
    }
}
